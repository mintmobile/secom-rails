# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151103163306) do

  create_table "eventos", force: :cascade do |t|
    t.text     "descricao",      limit: 65535
    t.text     "palestrante",    limit: 65535
    t.string   "horario",        limit: 255
    t.string   "dia",            limit: 255
    t.string   "codigo_horario", limit: 255
    t.string   "sala",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "codigo_sala",    limit: 4
    t.integer  "vagas",          limit: 4
  end

  create_table "participante_eventos", force: :cascade do |t|
    t.integer  "participante_id", limit: 4
    t.integer  "evento_id",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "participante_eventos", ["evento_id"], name: "index_participante_eventos_on_evento_id", using: :btree
  add_index "participante_eventos", ["participante_id"], name: "index_participante_eventos_on_participante_id", using: :btree

  create_table "participantes", force: :cascade do |t|
    t.string   "nome",               limit: 255
    t.string   "rg",                 limit: 255
    t.string   "cep",                limit: 255
    t.string   "logradouro",         limit: 255
    t.string   "numero",             limit: 255
    t.string   "complemento",        limit: 255
    t.string   "cidade",             limit: 255
    t.string   "estado",             limit: 255
    t.string   "telefone_celular",   limit: 255
    t.string   "telefone_fixo",      limit: 255
    t.string   "escolaridade",       limit: 255
    t.string   "curso",              limit: 255
    t.string   "instituicao",        limit: 255
    t.boolean  "servidor_municipal"
    t.string   "unidade_trabalho",   limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "tipo_inscricao",     limit: 255
    t.string   "email",              limit: 255
  end

  add_index "participantes", ["tipo_inscricao"], name: "index_participantes_on_tipo_inscricao", using: :btree

  create_table "salas", force: :cascade do |t|
    t.text     "descricao",   limit: 65535
    t.text     "palestrante", limit: 65535
    t.integer  "horario",     limit: 4
    t.integer  "numero",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_foreign_key "participante_eventos", "eventos"
  add_foreign_key "participante_eventos", "participantes"
end
