class CreateParticipanteEventos < ActiveRecord::Migration
  def change
    create_table :participante_eventos do |t|
      t.belongs_to :participante, index: true, foreign_key: true
      t.belongs_to :evento, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
