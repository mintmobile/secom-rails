class AddEmailToParticipantes < ActiveRecord::Migration
  def change
    add_column :participantes, :email, :string
  end
end
