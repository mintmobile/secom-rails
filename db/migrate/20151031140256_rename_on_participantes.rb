class RenameOnParticipantes < ActiveRecord::Migration
  def change
    rename_column :participantes, :cpf, :rg
  end
end
