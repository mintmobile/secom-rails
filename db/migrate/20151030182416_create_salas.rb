class CreateSalas < ActiveRecord::Migration
  def change
    create_table :salas do |t|
      t.text :descricao
      t.text :palestrante
      t.integer :horario
      t.integer :numero

      t.timestamps null: false
    end
  end
end
