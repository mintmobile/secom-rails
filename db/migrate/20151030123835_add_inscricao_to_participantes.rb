class AddInscricaoToParticipantes < ActiveRecord::Migration
  def change
    add_column :participantes, :tipo_inscricao, :string
    add_index :participantes, :tipo_inscricao
  end
end
