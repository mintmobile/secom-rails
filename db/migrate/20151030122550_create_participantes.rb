class CreateParticipantes < ActiveRecord::Migration
  def change
    create_table :participantes do |t|
      t.string :nome
      t.string :cpf
      t.string :cep
      t.string :logradouro
      t.string :numero
      t.string :complemento
      t.string :cidade
      t.string :estado
      t.string :telefone_celular
      t.string :telefone_fixo
      t.string :escolaridade
      t.string :curso
      t.string :instituicao
      t.boolean :servidor_municipal
      t.string :unidade_trabalho

      t.timestamps null: false
    end
  end
end
