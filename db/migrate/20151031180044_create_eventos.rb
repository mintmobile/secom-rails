class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.string :descricao
      t.string :palestrante
      t.string :horario
      t.string :dia
      t.string :codigo_horario
      t.string :sala

      t.timestamps null: false
    end
  end
end
