class ChangePalestranteOnEventos < ActiveRecord::Migration
  def change
    change_column :eventos, :palestrante, :text
    change_column :eventos, :descricao, :text
  end
end
