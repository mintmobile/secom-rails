collection @participantes
attributes :nome, :email, :rg, :cep, :logradouro, :numero, :complemento, :cidade, :estado, :telefone_celular, :telefone_fixo, :escolaridade, :curso, :instituicao, :servidor_municipal, :unidade_trabalho
child :eventos do |participantes|
  attributes :descricao, :sala, :vagas
end