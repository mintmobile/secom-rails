class Participante < ActiveRecord::Base

  validates_presence_of :nome, :rg, :telefone_celular, :email
  validates :email, confirmation: true
  validates_uniqueness_of :email, :on => :create, :message => "já está inscrito no evento."
  validates_uniqueness_of :telefone_celular, :on => :create, :message => "já está inscrito no evento."
  validates :email_confirmation, presence: true
  has_many :participante_eventos, dependent: :destroy
  has_many :eventos, through: :participante_eventos

  def todos_eventos
    eventos.map { |e| e.descricao + " " + e.sala }.join(" // ")
  end

  ESCOLARIDADES = ["Primeiro Grau Completo", "Primeiro Grau Incompleto", "Segundo Grau Incompleto", "Segundo Grau Completo", "Superior Incompleto", "Superior Completo", "Especialização", "Mestrado", "Doutorado"]

end
