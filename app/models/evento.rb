class Evento < ActiveRecord::Base

  def self.read_csv(file)
    CSV.foreach(file) do |row|
      Evento.create(descricao: row[0], palestrante: row[1], codigo_sala: row[2], codigo_horario: row[3], sala: row[4], horario: row[5], dia: row[6], vagas: row[7])
    end
  end

  has_many :participante_eventos

  def lotado?
    vagas_restantes <= 0
  end

  def vagas_restantes
    vagas - participante_eventos.count
  end

end
