class ParticipantesController < ApplicationController

  before_action :set_eventos

  def index
    redirect_to new_participante_path
  end

  def new
    render :encerrado
    #@participante = Participante.new
  end

  def busca
  end

  def buscar
    if @participante = Participante.find_by(rg: params[:rg])
      redirect_to "http://api.html2pdfrocket.com/pdf?value=#{certificado_participante_url(@participante)}&apikey=d8469d61-c254-4ef9-b3bc-0b133ea94990"
    else
      flash.now[:error] = "RG não encontrado."
      render :busca
    end
  end

  def certificado
    @participante = Participante.find(params[:id])
    render layout: nil
  end

  def create
    @participante = Participante.new(participante_params)
    if @participante.save
      params[:evento].each do |key, value|
        @participante.participante_eventos.create(evento_id: key) if value == "1"
      end
      if @participante.eventos.empty?
        flash.now[:error] = 'Inscrição não foi realizada - Você precisa selecionar ao menos 1 item da programação'
        @participante.destroy
        render :new
        return
      end
      ParticipantesMailer.confirmation_email(@participante).deliver_now
      render :successo
    else
      flash.now[:error] = 'Nao foi possivel realizar sua inscrição. Verifique os campos com erro.'
      render :new
    end
  end

  def export
    @participantes = Participante.all
  end

  def relatorio
    @eventos = Evento.all
  end

  def relatorio_participantes
    @participantes = Participante.all
  end

  private

  def set_eventos
    @eventos_array = []
    0.upto 10 do |i|
      eventos = Evento.where(codigo_horario: i)
      @eventos_array.append(eventos) unless eventos.empty?
    end
  end

  # o poder e a imprensa -- novos rumos da publicidade
  # twitter e política - sozinho
  # a comunicacao do morro sozinha
  # carreira e tecnologia - casada com ativismo social e a midia

  def participante_params
    params.require(:participante).permit(
      :nome, :rg, :email, :email_confirmation,
      :cep, :cidade, :estado, :logradouro, :complemento, :numero,
      :telefone_fixo, :telefone_celular,
      :servidor_municipal, :unidade_trabalho,
      :escolaridade, :curso, :instituicao
    )
  end

end
