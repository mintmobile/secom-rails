$(function() {

  function limpa_formulário_cep() {
  // Limpa valores do formulário de cep.
    $(".logradouro").val("");
    $(".cidade").val("");
    $(".estado").val("");
  }

  $(".cpf").inputmask("999.999.999-99");
  $(".cep").inputmask("99999-999");
  $(".phone").inputmask("(99) 99999999[9]");

  $(".cep").blur(function() {
    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            $(".logradouro").val("Pesquisando...")
            $(".cidade").val("Pesquisando...")
            $(".estado").val("Pesquisando...")

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $(".logradouro").val(dados.logradouro);
                    $(".cidade").val(dados.localidade);
                    $(".estado").val(dados.uf);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulário_cep();
                    $(this).val("");
                    $(".logradouro").val("CEP não encontrado.")
                    $(".cidade").val("CEP não encontrado.")
                    $(".estado").val("CEP não encontrado.")
                }
            });
        } //end if.
        else {
            //cep é inválido.
            $(this).val("");
            limpa_formulário_cep();
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
    });
  });