class ParticipantesMailer < ApplicationMailer

  def confirmation_email(participante)
    @participante = participante
    mail(to: @participante.email, subject: 'Confirmação de inscrição na Semana Internacional de Comunicação')
  end

end