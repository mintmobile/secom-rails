module ApplicationHelper

  def bootstrap_class_for flash_type
    { success: 'alert-success', notice: 'alert-success', error: 'alert-danger', warning: 'alert-warning'}[flash_type.to_sym]
  end

  def abbreviate_if_needed (full_name, length = 23)
    return full_name if full_name.length < length
    name_array = full_name.split(" ")
    name_array.each do |name|
      name.replace(name[0].chr + ".") if name.length >= 3 && name != name_array.first && name != name_array.last
      break if name_array.join(" ").length < length
    end
    return name_array.join(" ")
  end

end
