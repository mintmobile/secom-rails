Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'participantes#new'

  get :relatorio, to: 'participantes#relatorio'
  get :relatorio_participantes, to: 'participantes#relatorio_participantes'
  get :export, to: 'participantes#export'

  resources :participantes, only: [:index, :new, :create] do
    collection do
      get 'busca'
      post 'buscar'
    end
    member do
      get 'certificado'
    end
  end

end
